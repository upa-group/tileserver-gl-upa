# TileServer GL - UPA

[![Build Status](https://travis-ci.org/maptiler/tileserver-gl.svg?branch=master)](https://travis-ci.org/maptiler/tileserver-gl)
[![Docker Hub](https://img.shields.io/badge/docker-hub-blue.svg)](https://hub.docker.com/r/maptiler/tileserver-gl/)

Vector and raster maps with GL styles. Server side rendering by Mapbox GL Native. Map tile server for Mapbox GL JS, Android, iOS, Leaflet, OpenLayers, GIS via WMTS, etc.

## **Note**: This is a new version with custom style files for **Egypt** to be used in **UPA** projects.

## **Before Getting Started**

Make sure you have Node.js version `(>=10 and <11)` installed, **`v10.24.1`** is recommended (running `node -v` it should output something like `v10.24.1`).

If you have another installed version of NodeJS, try to install **`nvm`** (Node Versions Manager) and set `v10.24.1` a the `default version` for this repository.

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
source ~/.bashrc
```
Now install NodeJs v10.24.1, just type:
```bash
nvm install v14.10.0
```

You can see the different versions you have installed by typing:
```bash
nvm list
```
This shows the currently active version on the first line (-> v10.24.1), followed by some named aliases and the versions that those aliases point to.

## Get Started

Install `tileserver-gl` with server-side raster rendering of vector tiles with npm

```bash
npm install -g tileserver-gl
```

Start `tileserver-gl` with the `Egypt` vector tiles from our custom `config.json`, just call:

```bash
tileserver-gl
```
If everything is ok, you should see something like the following:

```bash
Starting tileserver-gl v3.1.1
Using specified config file from config.json
Starting server
Listening at http://[::]:8080/
Startup complete
```